##################### Employee Details ######################

import openpyxl                                     # Python library to read/write Excel files 
import sys                                          # Provides access to some variables used or maintained by the interpreter

from pathlib import Path                            # Represents a filesystem path


def employeeDetails(_id,column_name):

    xlsx_file = Path('.', 'EmployeeDetails.xlsx')   # Setting the path to the xlsx file:

    wb_obj = openpyxl.load_workbook(xlsx_file)      # Returns a value of the workbook data type

    sheet = wb_obj.active                           # Get workbook active sheet object

    col_names = []                                  # To get column headings or names
    for column in sheet.iter_cols(1, sheet.max_column): # from 1st column to last column 
        col_names.append(column[0].value)

    # print(col_names)
    index = None
    if column_name in col_names:
        index = col_names.index(column_name,0)     # To get the required column name from list
        # print(index)

    row_values = []
    for row in sheet.iter_rows(1,sheet.max_row):
        row_values.append(row[0].value)
       
        if (_id == str(row[0].value)):
            print("EmployeeName : ",row[1].value)
            if (index == None):                    # if valid column name is not present
                pass
            else:
                print(column_name ,":",row[index].value)
    
result = employeeDetails(sys.argv[1],sys.argv[2])  #Calling the function